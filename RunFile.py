# -*- coding: utf-8 -*-
'''
Run file for the receding-horizon MDP state exploration procedure
Originally coded by T.S. Badings, December 2020, Radboud University

Main paper reference:
	Badings, T.S., Hartmanns, A., Jansen, N. and Suilen, M. (2020). Balancing 
    Wind and Batteries: Toward Predictive Verification of Smart Grids. 
    NFM 2021.
'''

#------------------------------------------------------------------------------
# IMPORT PACKAGES
#------------------------------------------------------------------------------
import numpy as np
from datetime import timedelta, datetime
import pandas as pd
import matplotlib.pyplot as plt
import os

# Main classes and methods used
from core.preprocessing.initialize_results import initialize_results

from core.commons import tic, calc_freq_integral
from core.main import initialize_model, main_simulation

from core.postprocessing.save_results import save_data, save_plots, \
                              plot_wind_transition, save_outer_loop

# Make sure the matplotlib generates editable text (e.g. for Illustrator)
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

# Set random number generator seed (if desired) and start timer
# np.random.seed( 1933 )
tic()

setup           = dict()

# Retreive working folder
setup['folder']             = dict()
setup['folder']['base']     = os.getcwd()
setup['folder']['data']     = setup['folder']['base']+'/data'
setup['folder']['output']   = setup['folder']['base']+'/output'

setup['time']               = dict()

print('\n+++++++++++++++++++++++++++++++++++++++++++++++++++++\n')
print('EXPLORATION PROGRAM STARTED AT \n'+datetime.now().strftime("%m-%d-%Y %H-%M-%S"))
print('\n+++++++++++++++++++++++++++++++++++++++++++++++++++++\n')

#--------------------------------------------------------------------------
# USER INTERFACE
#--------------------------------------------------------------------------

setup['ui'] = dict()

print('CHOOSE YOUR NETWORK CONFIGURATION')
print('Press 1 for the 1-node, 1 battery case')
print('Press 2 for the 3-node, 1 battery case')
print('Press 3 for the 3-bus, 2 battery case')
setup['ui']['network'] = 0
while setup['ui']['network'] not in [1,2,3]:
    setup['ui']['network']  = int(input('Please choose your option: '))

print('\n+++++++++++++++++++++++++++++++++++++++++++++++++++++\n')

print('CHOOSE YOUR EXPLORATION HORIZON')
print('Press 1 for 300 seconds')
print('Press 2 for 600 seconds')
print('Press 3 for 900 seconds')

setup['ui']['explorationHor'] = 0
while setup['ui']['explorationHor'] not in [1,2,3]:
    setup['ui']['explorationHor']  = int(input('Please choose your option: '))
    
print('\n+++++++++++++++++++++++++++++++++++++++++++++++++++++\n')

print('CHOOSE YOUR ACTION DISCRETIZATION LEVEL')
setup['ui']['lambda']  = int(input('Please choose your option: '))
print('Discretization level of lambda =',setup['ui']['lambda'],'chosen')

print('\n+++++++++++++++++++++++++++++++++++++++++++++++++++++\n')

print('CHOOSE NUMBER OF MONTE CARLO ITERATIONS')
setup['ui']['iterations']  = int(input('Please choose your option: '))
print('Number of iterations chosen is',setup['ui']['iterations'])

print('\n+++++++++++++++++++++++++++++++++++++++++++++++++++++\n')

#--------------------------------------------------------------------------
# DEFINE OUTER LOOP SETTINGS
#--------------------------------------------------------------------------

# Number of iterations
setup['iterations']                 = setup['ui']['iterations']

# If below is FALSE, only plots for the last iteration are generated
setup['plot_every_iteration']       = True

#--------------------------------------------------------------------------
# DEFINE MAIN SIMULATION CASE SETTINGS
#--------------------------------------------------------------------------

# Set the number of points used for discretizing the continuous action space
setup['discretizationResolution']   = setup['ui']['lambda']

# Define optimization horizon [s]
if setup['ui']['explorationHor'] == 3:
    setup['time']['t_hor'] = 900 # seconds
elif setup['ui']['explorationHor'] == 2:
    setup['time']['t_hor'] = 600 # seconds
else:
    setup['time']['t_hor'] = 300 # seconds

# Retreive simulation time step and define indices for simulation of system
setup['time']['h_sim']              = 300 # Simulation time step [s]

#--------------------------------------------------------------------------
# DEFINE OTHER MAIN USER SETTINGS
#--------------------------------------------------------------------------

# Specify the simulation horizon 
setup['time']['sim_horizon']        = timedelta( hours=24 ) # Simulation horizon

# Define maximum allowed frequency deviations (in Hz)
setup['dfreq']                      = dict()
setup['dfreq']['min']               = -0.1
setup['dfreq']['max']               = 0.1

# Nr of time steps for initialization, in which no freq. limits are imposed
setup['initialization_time']        = 3

# The optimization criterium can be either of the following two values:
#   - probability: the probability of reaching a good leaf node is maximized
#   - reward: the expected reward is maximized (as defined in the value 
#     iteration function)
setup['optimization_criterium']     = 'reward'

# Discount factor the value iteration
setup['discount_factor']            = 1.0

# Wind p.u. base power in MW (to convert from and to p.u. values)
# Note: this is simply a scaling factor, to achieve a desired wind power 
# penetration level
setup['windBasePower']              = 20

# If verbose is TRUE, more detailed output is printed
setup['verbose']                    = False

# If TRUE, the 3d histogram for the wind transition model is produced
setup['create_wind_transition_plot'] = True

# Retreive datetime string
setup['datestring_start'] = datetime.now().strftime("%m-%d-%Y %H-%M-%S") + \
                            ' t_hor='+str(setup['time']['t_hor']) + \
                            ' h_sim='+str(setup['time']['h_sim'])

#------------------------------------------------------------------------------
# DEFINE FILE SETTINGS
#------------------------------------------------------------------------------

setup['files']          = dict()

# Result output excel filename
setup['files']['output'] = setup['datestring_start']+'.xlsx'

if setup['ui']['network'] == 1:
    # 1 node, 1 battery
    
    # Data files for the grid and storage units
    setup['files']['grid']  = 'grid_data_1bus.xlsx' 
    setup['files']['stor']  = 'storage_data_ns=1_1bus.xlsx'

    # Define other files to load: miscellaneuous load, wind power
    # forecast, and the transition function for the wind poewr model
    setup['files']['timeseries']          = {'miscellaneous_load': 'load_ENTSO-e_NL_scaled_18-11-2020_1bus.csv',
                                             'wind_forecast': 'wind_forecast.csv'}
    
elif setup['ui']['network'] == 2:
    # 3 nodes, 1 battery
    
    # Data files for the grid and storage units
    setup['files']['grid']  = 'grid_data_3bus.xlsx' 
    setup['files']['stor']  = 'storage_data_ns=1.xlsx'

    # Define other files to load: miscellaneuous load, wind power
    # forecast, and the transition function for the wind poewr model
    setup['files']['timeseries']          = {'miscellaneous_load': 'load_ENTSO-e_NL_scaled_18-11-2020_3bus.csv',
                                             'wind_forecast': 'wind_forecast.csv'}
    
else:
    # 3 nodes, 2 batteries
    
    # Data files for the grid and storage units
    setup['files']['grid']  = 'grid_data_3bus.xlsx' 
    setup['files']['stor']  = 'storage_data_ns=2.xlsx'

    # Define other files to load: miscellaneuous load, wind power
    # forecast, and the transition function for the wind poewr model
    setup['files']['timeseries']          = {'miscellaneous_load': 'load_ENTSO-e_NL_scaled_18-11-2020_3bus.csv',
                                             'wind_forecast': 'wind_forecast.csv'}
    
# File containing the wind transition model of the DTMC
setup['files']['transitionFunctions'] = {'wind_power': 'WindTransitionFunction_300sec_entso-e.xlsx'}

#--------------------------------------------------------------------------
# DEFINE TIME CONSTANTS
#--------------------------------------------------------------------------

# Specify the start date and optimization horizon (in seconds)
setup['time']['start_date']     = datetime( year=2020, month=11, day=18 )
setup['time']['opt_horizon']    = timedelta(seconds = setup['time']['t_hor'])

# Define end date and end date plus optimization horizon (used for timeseries)
setup['time']['end_date']       = setup['time']['start_date'] + \
                    setup['time']['sim_horizon']
setup['time']['end_date_plus_horizon'] = setup['time']['end_date'] + \
                    setup['time']['opt_horizon']
setup['time']['end_date_plus_horizonHour'] = setup['time']['end_date'] + \
                    max(setup['time']['opt_horizon'], timedelta(hours=1))

setup['time']['timedelta_sim']  = timedelta(seconds = setup['time']['h_sim'])
setup['time']['date_index_sim'] = pd.date_range(start=setup['time']['start_date'], 
                    end=setup['time']['end_date'], 
                    freq='{}S'.format(setup['time']['h_sim']))
setup['time']['date_index_sim_plus_horizon'] = pd.date_range(start=setup['time']['start_date'], 
                    end=setup['time']['end_date_plus_horizon'], 
                    freq='{}S'.format(setup['time']['h_sim']))

# Calculate number of steps in optimization horizon
setup['time']['Nhor'] = int(setup['time']['t_hor'] / setup['time']['h_sim'])
    
#------------------------------------------------------------------------------
# PLOT FONT SIZES
#------------------------------------------------------------------------------

# Plot font family and size
plt.rc('font', family='serif')
plt.ion()
SMALL_SIZE = 7
MEDIUM_SIZE = 9
BIGGER_SIZE = 9

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


montecarlo = dict()
montecarlo['overall_cost'] = np.zeros(setup['iterations'])
montecarlo['mdp_cost'] = pd.DataFrame(index=setup['time']['date_index_sim'])
montecarlo['nr_states'] = pd.DataFrame(index=setup['time']['date_index_sim'])
montecarlo['nr_actions'] = pd.DataFrame(index=setup['time']['date_index_sim'])
montecarlo['iteration_time'] = pd.DataFrame(index=setup['time']['date_index_sim'])
results = dict()

# Initialize the project
sys = initialize_model(setup)

# Plot wind transition function
if setup['create_wind_transition_plot']:
    plot_wind_transition(setup, sys['wind']['model']['P_tr'])

#------------------------------------------------------------------------------
# OUTER LOOP FOR MONTE CARLO VALIDATION
#------------------------------------------------------------------------------

# For every iteration
for it in range(setup['iterations']):
    
    print('<<< Start iteration',it,'>>>')
    
    # Close all figure windows
    plt.close('all')
    
    # Initialize variables associated with results
    results[it] = initialize_results(setup, sys)
    
    # Run the exploration procedure
    mdp, results[it] = main_simulation(setup, sys, results[it])
    
    # Store results in Excel file
    save_data(setup, results[it], it)
    
    # Compute trajectory results
    rows = len(setup['time']['date_index_sim'])
    flat_results = results[it]['x']['g'].to_numpy()[:rows, sys['size'].t : 2*sys['size'].t].flatten()
    width = 3600 / setup['time']['h_sim']
    montecarlo['overall_cost'][it] = calc_freq_integral(width, flat_results)
    
    # Store results for this iteration
    montecarlo['mdp_cost'][it]          = results[it]['mdp']['opt_objective']
    montecarlo['nr_states'][it]         = results[it]['mdp']['nr_states']
    montecarlo['nr_actions'][it]        = results[it]['mdp']['nr_actions']
    montecarlo['iteration_time'][it]    = results[it]['time']['iteration']

    if setup['plot_every_iteration'] is True:
        # Generate and save plots
        save_plots(setup, sys, results[it], it)

if setup['plot_every_iteration'] is False:
    # Generate and save plots
    save_plots(setup, sys, results[it], it)

#------------------------------------------------------------------------------
# %% PROGRAM ENDING
#------------------------------------------------------------------------------

import pickle

# Store outerloop results to Excel
save_outer_loop(setup, montecarlo)

# Set filename
filename = setup['folder']['output']+'/'+ str(setup['iterations'])+ \
    '_iterations_'+setup['datestring_start']+'_data.pickle'

# Save data in Pickle file
with open(filename, 'wb') as f:
    pickle.dump([montecarlo, results], f)
    f.close
    
print('\nData successfully saved in file "'+str(filename)+'"')

datestring_end = datetime.now().strftime("%m-%d-%Y %H-%M-%S")    
         
print('\n+++++++++++++++++++++++++++++++++++++++++++++++++++++\n')
print('APPLICATION FINISHED AT', datestring_end)