# -*- coding: utf-8 -*-
"""
This file contains the general workflow of the MDP exploration tool
"""
import numpy as np
import pandas as pd
import itertools 
import copy

from datetime import datetime

from .commons import network_size, tic, toc

from .preprocessing.import_from_excel import importGrid, importStor, \
    import_csv_timeseries, import_transition_model
from .preprocessing.define_grid import define as define_grid
from .preprocessing.define_storage import define as define_storage
from .preprocessing.define_wind import define as define_wind

def _import_data(setup, data=dict()):
    '''
    Import the power system model data from the various input files

    Parameters
    ----------
    setup : dict
        Dictionary containing settings
    data : dict, optional
        The initial data dictionary to fill in. The default is dict().

    Returns
    -------
    data : dict
        Dictionary containing all data

    '''
    
    # Load grid case data
    data['grid']       = importGrid(setup['folder']['data']+'/'+
                                    setup['files']['grid'])
    data['grid']['s']  = 1         # Gear order for grid discretization
        
    # Load storage case data
    data['stor']       = importStor(setup['folder']['data']+'/'+
                                    setup['files']['stor'])
    data['stor']['s']  = 1         # Gear order for storage discretization
    
    # Import timeseries from CSV files
    data['timeseries'] = import_csv_timeseries(setup['time'],
                           setup['folder']['data']+'/'+'timeseries'+'/',
                               setup['files']['timeseries'])
    
    # Load transition function of the uncertain variables
    data['windModel'] = import_transition_model(setup['folder']['data']+'/'+
                        setup['files']['transitionFunctions']['wind_power'])
    
    return data

def define_action_space(setup, sys):
    '''
    Define the full (naive) discretized action space. Currently, the action 
    space only spans the storage flexibility decisions of every individual 
    storage unit.
        
    Parameters
    ----------
    setup : dict
        Dictionary containing settings
    sys : dict
        Dictionary containing al info on the continuous model
    
    Returns
    ----------
    action_list : list of tuples
        List of tuples of all possible combinations of discrete action, i.e.
        the full discrete action space
    '''
    
    # Nr of discretization steps in each action dimension
    res = setup['discretizationResolution']
            
    # Create empty dictionaries
    actions = dict()
    action_list = dict()
    
    # Storage variables
    actions['S_stor'] = np.linspace(-sys['stor']['constr']['S_stor_dd'],
                                    sys['stor']['constr']['S_stor_id'], res).T
    
    for key, value in actions.items():
        if len(value.shape) > 1:
            action_list[key] = list(itertools.product(*value))
        else:
            action_list[key] = list(value)
            
    return action_list

def initialize_model(setup):   
    '''
    Initializes the power system model, given the specified setup. Returns 
    the dictionary sys, which is the main model dictionary that contains all
    data required for the simulations.

    Parameters
    ----------
    setup : dict
        Dictionary containing settings

    Returns
    -------
    sys : dict
        Dictionary containing al info on the continuous model
    '''
    
    # Dictionary 'sys' contains all relevant problem information
    sys            = dict()
    
    # Import data from Excel
    data = _import_data(setup)
    
    # Define and discretize subproblems
    sys['stor'], sys['dstor']   = define_storage( setup, data['stor'] )
    sys['grid'], sys['dgrid']   = define_grid( setup, data['grid'],
                                     sys['stor']['ns'], data['stor']['stor'],
                                     data['timeseries'] )
    sys['wind']                 = define_wind( setup, sys['grid']['nw'],
                                     data['timeseries']['wind_forecast'])
    sys['wind']['model']        = data['windModel']
    
    # Shortcuts for network size parameters
    sys['size'] = network_size(ng = sys['grid']['ng'], nt = sys['grid']['nt'], 
                     nw = sys['grid']['nw'], ns = sys['stor']['ns'])
    
    # Retreive maximum allowed frequency deviations (in Hz)
    sys['grid']['constr']['freqMin'] = [setup['dfreq']['min']]*sys['size'].t
    sys['grid']['constr']['freqMax'] = [setup['dfreq']['max']]*sys['size'].t
    
    sys['grid']['constr']['x_g0'] = np.concatenate(( np.zeros(2*sys['size'].t),  
                                      sys['grid']['constr']['P_GR0'] ))
    
    # Explore and define the complete (unconstrained) action space
    sys['action_list'] = define_action_space(setup, sys)
    
    return sys

def main_simulation(setup, sys, results):
    '''
    Main function to explore the given model over the full time horizon. Takes
    the setup, system, and results dictionaries as inputs. Returns the MDP
    model dictionary, and the result dictionary.

    Parameters
    ----------
    setup : dict
        Dictionary containing settings
    sys : dict
        Dictionary containing al info on the continuous model
    results : dict
        Dictionary containing all results

    Returns
    -------
    mdp : dict
        Main MDP model dictionary
    results : dict
        Dictionary containing all results

    '''
    
    mdp = dict()
    
    # Evaluate the initial wind power state            
    Pw_state = np.argmin(abs(sys['wind']['model']['states']))
    print('Wind state is',Pw_state)
    
    # Save the wind error state to the results
    t0 = setup['time']['date_index_sim']
    results['x']['wind_error_state'].loc[t0] = Pw_state

    print(' - Calling MDP state-space exploration programme...')
    mdp, results = mdp_simulation(setup, sys, results, mdp)
    
    return mdp, results

def mdp_simulation(setup, sys, results, mdp):
    
    from .explore_model import explore_tree
    from .solve_model import value_iteration, act_and_reduce
    
    for i,t in enumerate(setup['time']['date_index_sim']):
        # Simulate step
        
        toc(False)
        
        t_plus_horizon = t + setup['time']['opt_horizon']
        print('\n > Current time is: {}'.format(datetime.now()))
        print(' - Explore state-space at t = ',t)
        
        # Time step array to consider in simulation
        freq_sim = '{}S'.format(setup['time']['h_sim'])
        time_steps = pd.date_range(start=t, end=t_plus_horizon, freq=freq_sim)
        
        if i == 0:
            # In first iteration, the complete model tree must be built
            
            # Retreive initial conditions
            xg0         = results['x']['g'].loc[t]
            xs0         = results['x']['s'].loc[t]
            Pw_state    = int(results['x']['wind_error_state'].loc[t])
            
            # Define dictionary to store exploration results as an MDP
            mdp['nr_states']             = int(0)
            mdp['nr_actions']            = int(0)
            mdp['states']                = [None]*100
            mdp['enabled_actions']       = [[0]]*100
            mdp['control_input']         = [[0]]*100
            mdp['goodLeafNodes']         = []
            mdp['badStates']             = []
            mdp['trans']                 = dict()
            
            # Add the initial state dictionary    
            state = {'xg': tuple(xg0), 
                     'xs': tuple(xs0), 
                     'wind_error': Pw_state}
            mdp['states'][mdp['nr_states']] = {'state': state, 
                                               'time': time_steps[0], 
                                               'timestep': i,
                                               'trace': [],
                                               'windtrace': []}
            stateID                      = 0
            mdp['nr_states']             += 1
            
            # Start state-space exploration procedure
            mdp = explore_tree(setup, mdp, time_steps, i, sys,
                    trace=[], windtrace=[], xg0=xg0,
                    xs0=xs0, Pw_state=Pw_state, stateID = stateID)
            
        else:
            # In subsequent iterations, we only have to add one layer to 
            # the model tree
            
            # Copy the dicitonary of current states (to avoid issues 
            # within the for-loop below)
            mdp_states = copy.copy(mdp['states'])
            mdp_goodLeafNodes = copy.copy(mdp['goodLeafNodes'])
            
            # Remove all leaf nodes from the list of end states (it
            # is not a leaf node anymore)
            mdp['goodLeafNodes'] = []
            
            # For every leaf node...
            for end_state in mdp_goodLeafNodes:
                # ... perform exploration for the new layer
                
                # Define initial conditions
                xg0         = mdp_states[end_state]['state']['xg']
                xs0         = mdp_states[end_state]['state']['xs']
                Pw_state    = mdp_states[end_state]['state']['wind_error']
                
                # Number of time steps for expanding the tree is one
                expand_steps  = pd.date_range(start=mdp['states'][end_state]['time'], \
                                           end=t_plus_horizon, freq=freq_sim)
                expand_timestep = mdp['states'][end_state]['timestep']
                    
                # Retreive the traces of the current node being extended
                trace       = mdp_states[end_state]['trace']
                windtrace   = mdp_states[end_state]['windtrace']
                
                # Perform exploration
                mdp = explore_tree(setup, mdp, expand_steps, expand_timestep, sys,
                    trace=trace, windtrace=windtrace, xg0=xg0,
                    xs0=xs0, Pw_state=Pw_state, stateID = end_state)
            
        # Remove none-types from mdp lists
        mdp['states']  = [x for x in mdp['states'] if x]
        
        print('\n - Model building completed')
        print(' --- Number of states:', mdp['nr_states'])
        print(' --- Number of good leaf nodes:', len(mdp['goodLeafNodes']))
        print(' --- Number of bad nodes:', len(mdp['badStates']))
        
        print('\n - Solve the MDP at t = ',t)
        
        # Solve the resulting MDP and derive the optimal policy
        results['mdp_full']['policy'][t], results['mdp_full']['opt_objective'][t] = \
            value_iteration(setup, sys['size'], mdp, time_steps)
          
        # And store the maximum objective (reward or probability)
        # for the current measurement state (state 0)
        results['mdp']['opt_objective'].loc[t] = \
            results['mdp_full']['opt_objective'][t][0]
        
        if setup['optimization_criterium'] == 'probability':
            if float(results['mdp']['opt_objective'].loc[t]) == 0:
                print('\n - The probability to keep system stable is zero, so abort')
                break
            else:
                print('\n - Probability to keep system stable is',
                      float(results['mdp']['opt_objective'].loc[t]))
                    
        else:
            print('\n - The maximum expected total reward is',
                  float(results['mdp']['opt_objective'].loc[t]))

        print('\n - Perform one of the actions', \
              results['mdp_full']['policy'][t][0],
              'and reduce the model at t = ',t)
        
        # If not in last time step, reduce model size
        if t <= setup['time']['date_index_sim'][-1]:
            try:
                # Perform the optimal action at the current time, proceed in time,
                # and eliminate unnecessary branches from the model tree
                mdp, results = act_and_reduce(sys['size'], mdp, results, time_steps[0:2],
                    actions=results['mdp_full']['policy'][t][0])
            except:
                print('Error while reducing model solve for next iteration')
                break
            
        # Store the time taken to perform this iteration
        results['time']['iteration'].loc[t] = toc(False)

    return mdp, results