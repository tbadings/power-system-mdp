# -*- coding: utf-8 -*-
import numpy as np
import scipy.linalg as sl

from core.preprocessing.define_gears_order import gears_order

def define(setup, data):
    '''
    Define the storage dictionaries

    Parameters
    ----------
    setup : dict
        Dictionary containing settings
    data : dict
        Input data

    Returns
    -------
    s : dict
        Dictionary of general storage data
    ds : TYPE
        Dictionary of discretized storage data
    '''
    
    # Create data dictionary
    s           = dict()
    s['mat']    = dict() 
    s['constr'] = dict()
    
    # Determine numbers of elements
    s['ns']     = len(data['stor'])  # Number of storage units
    s['base']   = np.array(data['base']) # Base p.u. in MVA
    
    #------------------------------------------------------------------------------
    # Set constraint parameters
    #------------------------------------------------------------------------------
    # Define storage constraint parameters
    s['constr']['x_s0']          = np.zeros( s['ns'] )  # Initial storage SoC [kWh]
    
    s['constr']['x_s_min']       = np.zeros( s['ns'] )  # Minimum buffer level [kWh]
    s['constr']['x_s_max']       = np.zeros( s['ns'] )  # Maximum buffer level [kWh]
    s['constr']['x_s_zeta']      = np.zeros( s['ns'] )  # Efficiency parameter (for state x_s)
    s['constr']['x_s_eta']       = np.zeros( s['ns'] )  # Efficiency parameter (for input P_s)
    
    s['constr']['S_stor_dd']     = np.zeros( s['ns'] )  # Decreased-demand flexibility scheduled [MW]
    s['constr']['S_stor_id']     = np.zeros( s['ns'] )  # Increased-demand flexibility scheduled [MW]
    
    for i,x in data['stor'].items():
        s['constr']['x_s0'][x['ID']]            = x['SoC0']
        
        s['constr']['x_s_max'][x['ID']]         = x['storMax']
        s['constr']['x_s_zeta'][x['ID']]        = x['storZeta']
        s['constr']['x_s_eta'][x['ID']]         = x['storEta']
        
        s['constr']['S_stor_dd'][x['ID']]       = x['S_stor_dd']
        s['constr']['S_stor_id'][x['ID']]       = x['S_stor_id']
        
    Ab_blocks       = dict()
    Bb_blocks       = dict() 
    
    for i in range(s['ns']):
        Ab_blocks[i] = np.array([[s['constr']['x_s_zeta'][i]-1]])
        Bb_blocks[i] = np.array([[s['constr']['x_s_eta'][i], s['constr']['x_s_eta'][i]]])

    Ab_build    = Ab_blocks[0]
    Bb_build    = Bb_blocks[0]

    for i in range(1,s['ns']):
        Ab_build    = sl.block_diag(Ab_build, Ab_blocks[i])
        Bb_build    = sl.block_diag(Bb_build, Bb_blocks[i])

    s['mat']['Ab']      = Ab_build
    s['mat']['Bb']      = Bb_build

    #------------------------------------------------------------------------------
    # Discretization of continuous system
    #------------------------------------------------------------------------------
    # Define discrete dictionary
    ds                                  = dict()
    ds['alpha'], beta0, ds['alphaSum']  = gears_order(data['s'])
    
    # Implicit (backward) Euler discretization w.r.t. time
    ds['A_bar']     = np.linalg.inv( np.eye(s['ns']) - setup['time']['h_sim']*beta0*s['mat']['Ab'] )
    ds['O_bar']     = setup['time']['h_sim']*beta0*ds['A_bar']  
    
    ds['A_sim']     = ds['A_bar'] * ds['alphaSum'] @ np.eye(s['ns'])  
    ds['B_sim']     = ds['O_bar'] @ s['mat']['Bb']
    
    return s, ds