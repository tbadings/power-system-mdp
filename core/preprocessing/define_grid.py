# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd

from core.preprocessing.define_gears_order import gears_order

def define(setup, data, ns, data_stor, data_timeseries):
    '''
    Define the grid dictionaries

    Parameters
    ----------
    setup : dict
        Dictionary containing settings
    data : dict
        Input data
    ns : int
        Number of storage units in the network
    data_stor : dict
        Data regarding storage units (to create incidence matrix)
    data_timeseries : dict
        Timeseries data (for load pattern, etc.)

    Returns
    -------
    s : dict
        Dictionary of general grid data
    ds : TYPE
        Dictionary of discretized grid data
    '''
    
    # Create data dictionary
    s           = dict()
    s['mat']    = dict() 
    s['constr'] = dict()
    
    # Determine numbers of elements
    s['nt']        = len(data['bus'])      # Number of buses
    s['ng']        = len(data['gen'])      # Number of generators
    s['nw']        = len(data['wind'])     # Number of wind farms
    s['ne']        = len(data['branch'])   # Number of branches (edges)
    s['base']      = np.array(data['base']) # Base p.u. in MVA
    
    #------------------------------------------------------------------------------
    # Set constraint parameters
    #------------------------------------------------------------------------------
    s['constr']['P_GR0']        = np.zeros( s['ng'] )
    s['constr']['R_GR0']        = np.zeros( s['ng'] )
    s['constr']['P_GR_min']     = np.zeros( s['ng'] )
    s['constr']['P_GR_max']     = np.zeros( s['ng'] )
    s['constr']['dP_GR_min']    = np.zeros( s['ng'] )
    s['constr']['dP_GR_max']    = np.zeros( s['ng'] )
    
    s['constr']['R_ds']         = np.zeros( s['ng'] )
    s['constr']['R_us']         = np.zeros( s['ng'] )
    
    for i,x in data['gen'].items():
        s['constr']['P_GR0'][x['ID']]      = x['P_GR0']
        s['constr']['R_GR0'][x['ID']]      = x['R_GR0']
        s['constr']['P_GR_min'][x['ID']]   = x['Pmin']
        s['constr']['P_GR_max'][x['ID']]   = x['Pmax']
        s['constr']['dP_GR_min'][x['ID']]  = x['dPmin']
        s['constr']['dP_GR_max'][x['ID']]  = x['dPmax']
        
        s['constr']['R_ds'][x['ID']]       = x['R_ds']
        s['constr']['R_us'][x['ID']]        = x['R_us']
        
    s['constr']['Lmax']     = np.zeros( s['ne'] )
    s['constr']['Lmin']     = np.zeros( s['ne'] )
        
    for i,x in data['branch'].items():   
        # Set line limits
        s['constr']['Lmax'][x['ID']]    = x['Lmax']
        s['constr']['Lmin'][x['ID']]    = x['Lmin']
    
    # Define general matrices
    s['mat']['Neighborhood'] = np.zeros((s['nt'], s['nt']))    # Define grid neighborhood matrix
    s['mat']['Susceptance']  = np.zeros((s['nt'], s['nt']))    # Define grid susceptance matrix
    s['mat']['Laplacian']    = np.zeros((s['ne'], s['nt']))    # Define Laplacian matrix (for power flows)
    
    # Copy generator data
    s['gen']                = data['gen']
    
    # Determine slack bus
    for i,x in data['bus'].items():
        if x['slack']:
            s['slackBus'] = x['ID']
            
    # For each grid branch
    for i,x in data['branch'].items():
        fbus = x['from']
        tbus = x['to']
        susc = x['b']
        i_id = x['ID']
        
        # Set values in neighborhood matrix
        s['mat']['Neighborhood'][ fbus, tbus ] = 1
        s['mat']['Neighborhood'][ tbus, fbus ] = 1
        
        # Set values in susceptance matrix
        s['mat']['Susceptance'][ fbus, tbus ] = susc
        s['mat']['Susceptance'][ tbus, fbus ] = susc
        
        # Set values in Laplacian matrix
        s['mat']['Laplacian'][ i_id, tbus ] = -susc
        s['mat']['Laplacian'][ i_id, fbus ] = susc
        
    # Define incidence matrices
    s['mat']['gen_to_grid']    = np.zeros((s['nt'], s['ng']))    # Define GR-grid incidence matrix
    s['mat']['wind_to_grid']   = np.zeros((s['nt'], s['nw']))    # Define wind-grid incidence matrix
    s['mat']['stor_to_grid']   = np.zeros((s['nt'], ns))    # Define stor-grid incidence matrix
        
    for i,x in data['gen'].items():
        # Set value in generator-grid incidence matrix
        s['mat']['gen_to_grid'][ x['bus'], x['ID'] ] = 1
        
    for i,x in data['wind'].items():
        # Set value in generator-grid incidence matrix
        s['mat']['wind_to_grid'][ x['bus'], x['ID'] ] = 1
        
    for i,x in data_stor.items():
        # Set value in storage incidence matrix
        s['mat']['stor_to_grid'][ x['bus'], i ] = 1
    
    # Define number of steps in optimization horizon
    Nhor    = int(setup['time']['t_hor']/setup['time']['h_sim'])
    NhorEye = np.eye(Nhor)    
    
    # Define Laplacian to calculate line power flow
    s['L']      = dict()
    s['L']      = np.kron( NhorEye, np.concatenate([
                                 s['mat']['Laplacian'],
                                 np.zeros((s['ne'],s['nt'])) ], axis=1) ) 
       
    s['timeseries']     = pd.DataFrame(index=setup['time']['date_index_sim_plus_horizon'])

    # Add timeseries for miscellaneous load
    for load in range(s['nt']):
        load_remainder = load % data_timeseries['miscellaneous_load'].shape[1]
        s['timeseries'].insert(0, value = data_timeseries['miscellaneous_load'][str(load_remainder)],
                               column='P_misc'+str(load) )
        
    s['P_misc'] = s['timeseries'].to_numpy()
    
    #------------------------------------------------------------------------------
    # Define continuous system
    #------------------------------------------------------------------------------
    # Define continuous matrices
    s['M'] = np.ones( s['nt'] )
    s['D'] = np.ones( s['nt'] )
    for i,x in data['bus'].items():
        if sum(s['mat']['gen_to_grid'][i,:]) + sum(s['mat']['wind_to_grid'][i,:]) > 0:
            s['M'][x['ID']] = x['M1']
            s['D'][x['ID']] = x['D1']
        else:  
            s['M'][x['ID']] = x['M0']
            s['D'][x['ID']] = x['D0']
            
    s['mat']['Eg']         = np.diag( np.concatenate(( np.ones(s['nt']), s['M'], np.ones(s['ng']) )) )
    s['mat']['PsiLin']     = np.zeros(( s['nt'], s['nt'] ))
    
    for k in range(s['nt']):
        for j in range(s['nt']):
            # Add neighborhood nodes to Psi mat
            if k == j:
                s['mat']['PsiLin'][k,j]     = sum(s['mat']['Susceptance'][k,:])
            else:
                s['mat']['PsiLin'][k,j] = -s['mat']['Susceptance'][k,j]
        
    # Fill grid system structure
    # concatenate Ag matrix
    
    # Define linear dynamics (power flow in the A_g matrix)
    s['mat']['Ag_lin']      = np.concatenate((
        np.concatenate(( np.zeros((s['nt'],s['nt'])),  np.eye(s['nt']),     np.zeros((s['nt'],s['ng'])) ), 1),
        np.concatenate(( -s['mat']['PsiLin'],          -np.diag(s['D']),    s['mat']['gen_to_grid']), 1),
        np.zeros((s['ng'],2*s['nt'] + s['ng']))
        ))
        
    # Define non-linear dynamics (power flow NOT in the A_g matrix)
    s['mat']['Ag_nonlin']      = np.concatenate((
        np.concatenate(( np.zeros((s['nt'],s['nt'])),  np.eye(s['nt']),     np.zeros((s['nt'],s['ng'])) ), 1),
        np.concatenate(( np.zeros((s['nt'],s['nt'])),  -np.diag(s['D']),    s['mat']['gen_to_grid']), 1),
        np.zeros((s['ng'],2*s['nt'] + s['ng']))
        ))
                            
    # Generator incidence matrix for system
    s['mat']['Bg']     = np.concatenate((
        np.zeros((       s['nt'],2*s['ng'] + s['nt'] + ns)),
        np.concatenate(( s['mat']['gen_to_grid'],    s['mat']['gen_to_grid'],     -np.eye(s['nt']),    -s['mat']['stor_to_grid'] ), 1),
        np.concatenate(( np.eye(s['ng']),           np.zeros((s['ng'],s['ng'] + s['nt'] + ns)) ), 1)
        ))
    
    # Wind incidence matrix for system
    s['mat']['Cg']     = np.concatenate(( np.zeros((s['nt'],s['nw'])), s['mat']['wind_to_grid'], np.zeros((s['ng'],s['nw'])) ))
    
    #------------------------------------------------------------------------------
    # Discretization of continuous system
    #------------------------------------------------------------------------------
    
    # Define discrete dictionary
    ds                                  = dict()
    ds['alpha'], beta0, ds['alphaSum']  = gears_order(data['s'])
    
    ds['A_bar']     = np.linalg.inv( s['mat']['Eg'] - setup['time']['h_sim']*beta0*s['mat']['Ag_lin'] )

    ds['O_bar']     = setup['time']['h_sim']*beta0*ds['A_bar']

    ds['A_sim']     = ds['A_bar'] * ds['alphaSum'] @ s['mat']['Eg']
    ds['B_sim']     = ds['O_bar'] @ s['mat']['Bg']
    ds['C_sim']     = ds['O_bar'] @ s['mat']['Cg']
    
    return s,ds