# -*- coding: utf-8 -*-

def define(setup, nw, windpower_data):
    '''
    Define the wind dictionary

    Parameters
    ----------
    setup : dict
        Dictionary containing settings
    nw : int
        Number of wind farms
    windpower_data : dict
        Timeseries data for the wind power forecast

    Returns
    -------
    s : dict
        Dictionary of wind power data
    '''
    
    s           = dict()

    # Retreive wind base poewr
    s['base'] = setup['windBasePower']

    s['Pw_f_vec'] = windpower_data.to_numpy().flatten() * s['base']
    
    return s