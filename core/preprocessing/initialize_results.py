# -*- coding: utf-8 -*-
import pandas as pd

def initialize_results(setup, sys):
    '''
    Add results dictionary
    
    Parameters
    ----------
    setup : dict
        Dictionary containing all settings
    sys : dict
        Dictionary containing all model data
    
    Returns
    ----------
    results : dict
        Dictionary containing all results
    '''
    
    results = dict()
    
    # Retreive start date
    start_date = setup['time']['start_date']
    
    #------------------------------------------------------------------------------
    # Initialize variables
    #------------------------------------------------------------------------------
    
    # Shortcuts to system size
    n = sys['size']
    
    results_fields = {
        'x':        {'g': 2*n.t + n.g,          # Grid state
                     's': n.s,                  # Storage state
                     'wind_error_state': 1},    # Wind error state
        'grid':     {'dP_disp': n.g,            # Power generator dispatch
                     'R': n.g,                  # Reserve deployment
                     'P_conv': n.t},            # Conventional load
        'stor':     {'P_stor': n.s,             # Storage power
                    'S_stor': n.s},             # Storage flex dispatch
        'wind':     {'wind_forecast': n.w,      # Wind forecast in MW
                     'wind_error': n.w},        # Wind error in MW
        'mdp':      {'action': 1,            # Action taken
                     'opt_objective': 1,     # Optimal cost/reward 
                     'nr_states': 1,         # Number of states
                     'nr_actions': 1},       # Number of actions
        'time':     {'iteration': 1}       # Time per iteration
        }
        
    # For every type of object
    for sys_type in results_fields:
        # Create new dictionary
        results[sys_type] = dict()
        # For every variable in that object
        for var,val in results_fields[sys_type].items():
            # Create empty dataframe
            _add_empty_dataframe(results, sys_type, var, 
                                 setup['time']['date_index_sim_plus_horizon'],
                                 val)
    
    # Grid initial states are assumed zero at the first time instance
    results['x']['g'].loc[start_date]        = sys['grid']['constr']['x_g0']
    results['x']['s'].loc[start_date]        = sys['stor']['constr']['x_s0']
        
    results['mdp_full']                  = dict()
    results['mdp_full']['policy']        = dict()
    results['mdp_full']['opt_objective'] = dict()
    
    return results

def _add_empty_dataframe(results_dict, sys_type, var, time_index, 
                         columns):
    '''
    Add empty results dataframe
    
    Parameters
    ----------
    results_dict : dict
        Dictionary containing all results
    sys_type : str
        String of system type
    var : str
        String of variable type within system
    time_index : timeseries
        Pandas timeseries for time indexing
    columns : str
        Number of columns in associated dataframe
    '''
        
    if columns > 1:
        columns = [str(var)+'_'+str(i) for i in range(columns)]
    else:
        columns = [str(var)]
    
    results_dict[sys_type][var] = pd.DataFrame(0, index=time_index, columns=columns)
    
    return results_dict