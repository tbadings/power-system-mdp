# -*- coding: utf-8 -*-
"""
Functions to import data from excel
"""

import xlrd
import pandas as pd
import numpy as np

from datetime import datetime, timedelta

def importGrid(file):
    '''
    Import Excel file for the power grid
    '''
    
    # Create data dictionary
    data = dict()
    
    # Open sheet
    wb = xlrd.open_workbook(file)    
    
    # Load general data
    general = dict()
    gnrl = wb.sheet_by_name('General')
    for i in range(gnrl.nrows):
        general[gnrl.cell_value(i,0)] = gnrl.cell_value(i,1)
        
    data['base'] = int(general['baseMVA'])
    
    # Load bus data
    data['bus'] = dict()
    sheet = wb.sheet_by_name('Buses')
    for i in range(sheet.nrows-1):
        data['bus'][i] = dict()
        for j in range(sheet.ncols):
            colVal = str(sheet.cell_value(0,j))
            data['bus'][i][colVal] = sheet.cell_value(i+1,j)
            if data['bus'][i][colVal].is_integer():
                data['bus'][i][colVal] = int(data['bus'][i][colVal])
            
    # Load generator data
    data['gen'] = dict()
    sheet = wb.sheet_by_name('Generators')
    for i in range(sheet.nrows-1):
        data['gen'][i] = dict()
        for j in range(sheet.ncols):
            colVal = sheet.cell_value(0,j)
            data['gen'][i][colVal] = sheet.cell_value(i+1,j)
            if data['gen'][i][colVal].is_integer():
                data['gen'][i][colVal] = int(data['gen'][i][colVal])
                
    # Load generator data
    data['wind'] = dict()
    sheet = wb.sheet_by_name('Wind')
    for i in range(sheet.nrows-1):
        data['wind'][i] = dict()
        for j in range(sheet.ncols):
            colVal = sheet.cell_value(0,j)
            data['wind'][i][colVal] = sheet.cell_value(i+1,j)
            if data['wind'][i][colVal].is_integer():
                data['wind'][i][colVal] = int(data['wind'][i][colVal])
            
    # Load branch data
    data['branch'] = dict()
    sheet = wb.sheet_by_name('Branches')
    for i in range(sheet.nrows-1):
        data['branch'][i] = dict()
        for j in range(sheet.ncols):
            colVal = sheet.cell_value(0,j)
            data['branch'][i][colVal] = sheet.cell_value(i+1,j)
            if data['branch'][i][colVal].is_integer():
                data['branch'][i][colVal] = int(data['branch'][i][colVal])
    
    return data

def importStor(file):
    '''
    Import Excel file for the storage units
    '''
    
    # Create data dictionary
    data = dict()
    
    # Open sheet
    wb = xlrd.open_workbook(file)    
    
    # Load general data
    general = dict()
    gnrl = wb.sheet_by_name('General')
    for i in range(gnrl.nrows):
        general[gnrl.cell_value(i,0)] = gnrl.cell_value(i,1)
        
    data['base'] = int(general['baseMVA'])

    # Load other data
    data['stor'] = dict()
    sheet = wb.sheet_by_name('StorageUnits')
    for i in range(sheet.nrows-1):
        data['stor'][i] = dict()
        for j in range(sheet.ncols):
            colVal = sheet.cell_value(0,j)
            data['stor'][i][colVal] = sheet.cell_value(i+1,j)
            if data['stor'][i][colVal].is_integer():
                data['stor'][i][colVal] = int(data['stor'][i][colVal])
           
    # Determine number of storage units
    data['ns'] = len(data['stor'])
                
    return data

def import_csv_timeseries(time, folder, file_list):
    '''
    Import timeseries from CSV files
    '''
    
    # Create dictionary for output timeseries
    timeseries = dict()
    
    # For each file in the file list
    for key, file in file_list.items():
        # try:
        # Read the CSV file using pandas
        print('\nLoading CSV file:',file)
        data = pd.read_csv(folder+file, sep=';')
        
        # Determine number of periods per day and define timedelta
        periods_per_day = max(data['period'])
        timedelta_per_period = timedelta(days = 1/periods_per_day)
        
        # Define dataframe for current timeseries
        timeseries[key] = pd.DataFrame(columns=data.columns)

        # Define empty list        
        rowdate = [None] * len(data)
        
        # For each row in the dataframe
        for index, row in data.iterrows():
            # Determine date
            period = int(row['period'] - 1)
            rowdate[index] = datetime( year=int(row['year']), month=int(row['month']), day=int(row['day']) ) + timedelta_per_period * period

        # Add list of dates to index of Pandas dataframe
        data.index=rowdate
        
        # Remove other columns associated with date
        timeseries[key] = data.drop(columns=['year','month','day','period'])
        
        # Remove columns with any NaN value from dataframe
        timeseries[key] = timeseries[key].dropna(axis=1, how='any')
        
        # Resample and interpolate timeseries data to match the simulation horizon and time step
        interpolate_method = 'linear'
        
        timeseries[key] = timeseries[key].resample(time['timedelta_sim'], \
                                                   label='right', closed='right').interpolate(method=interpolate_method)
            
        # Retreive simulation horizon
        horizon_idx = time['date_index_sim_plus_horizon']
            
        # Check if timeseries covers complete simulation timeframe
        timeseries_idx = timeseries[key].index
        if not all(i in timeseries_idx for i in horizon_idx):
            print(' >>> WARNING: timeseries',key,'does not cover full time horizon.')
            
            # Shift timeseries to make correspond with right start time
            time_shift = horizon_idx[0] - timeseries[key].index[0]
            print(' >>> Solved by shifting start time of timeseries by:',time_shift)
            
            # Copy values in timeseries
            df_values = timeseries[key].values
        
            # Reindex index of timeseries and copy values
            timeseries[key] = timeseries[key].reindex(timeseries_idx + time_shift)
            timeseries[key].loc[:] = df_values
            
        # If still not the complete simulation timeframe is covered, copy days
        count = 0
        if not all(i in timeseries_idx for i in horizon_idx):
            print(' >>> WARNING: timeseries',key,'does not contain sufficient data.')
            
            for t in horizon_idx:
                # Update timeseries index list
                timeseries_idx = timeseries[key].index
                
                # Check if index exists in timeseries
                if t not in timeseries_idx:
                    # If not, copy value from previous day
                    prev_day = t - timedelta(days = 1)
                    if prev_day in timeseries_idx:
                        if count == 0:
                            print(' >>> Timeseries pattern assumed constant for each day in horizon')
                        count += 1
                        timeseries[key].loc[t] = timeseries[key].loc[prev_day]
                    else:
                        print(' >>> ERROR AVOIDED: no data available to extrapolate from:')
                        print(' >>> Tried copying {} from {}'.format(t,prev_day))
                        timeseries[key].loc[t] = 0
            print(' >>> In total,',count,'rows were added to the timeseries')
            
        # Remove all rows outside of simulation horizon
        timeseries[key] = timeseries[key][ timeseries[key].index.isin(horizon_idx) ]

    return timeseries

def import_transition_model(file):
    '''
    Import Excel file as a matrix
    '''
    
    # Create data dictionary
    data = dict()
    data['P_tr'] = []
    
    # Open sheet
    wb = xlrd.open_workbook(file)   
    sheet = wb.sheet_by_name('Transitions')
    
    for row in range (sheet.nrows):
        _row = []
        for col in range (sheet.ncols):
            _row.append(sheet.cell_value(row,col))
        data['P_tr'].append(_row)
        
    data['P_tr'] = np.array(data['P_tr'])
            
    sheet = wb.sheet_by_name('States')
    
    _row = []
    for col in range (sheet.ncols):
        _row.append(sheet.cell_value(0,col))
    data['states'] = np.array(_row)
    
    data['nr_states'] = len(data['states'])
    
    data['successors'] = [-1]*data['nr_states']
    
    for i,s in enumerate(data['states']):
        data['successors'][i] = np.where(data['P_tr'][i,:] > 0)[0]
    
    return data