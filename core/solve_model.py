# -*- coding: utf-8 -*-

import numpy as np
from itertools import compress

from .commons import calc_reward

def _all_argmax(a):
    '''
    Similar to np.argmax, but returns all occurences of the maximum

    Parameters
    ----------
    a : list
        List to be evaluted

    Returns
    -------
    list
        Index list

    '''
    if len(a) == 0:
        return []
    all_ = [0]
    max_ = a[0]
    for i in range(1, len(a)):
        if a[i] > max_:
            all_ = [i]
            max_ = a[i]
        elif a[i] == max_:
            all_.append(i)
    return all_

def _all_argmin(a):
    '''
    Similar to np.argmin, but returns all occurences of the minimum

    Parameters
    ----------
    a : list
        List to be evaluted

    Returns
    -------
    list
        Index list

    '''
    if len(a) == 0:
        return []
    all_ = [0]
    min_ = a[0]
    for i in range(1, len(a)):
        if a[i] < min_:
            all_ = [i]
            min_ = a[i]
        elif a[i] == min_:
            all_.append(i)
    return all_

def value_iteration(setup, size_n, mdp, time_steps):
    '''
    Given the MDP model, solve it via backward value iteration

    Parameters
    ----------
    setup : dict
        Dictionary containing settings
    size_n : object
        Object containing network size values
    mdp : dict
        Dictionary containing all MDP model information
    time_steps : list
        List of datetime values corresponding to the time horizon

    Returns
    -------
    policy_vector : list
        Optimal policy when being in any state
    reward_vector : numpy 1d array
        Reachability probability when being in any state

    '''
    
    # Determine last time step
    reverse_time_steps = time_steps[::-1]

    # Define zero vectors for results
    reward_vector = np.zeros(mdp['nr_states'])
    policy_vector = [None]*mdp['nr_states']
    
    print(' --- Set initial rewards at',reverse_time_steps[0])
    
    # For every "good" end state, set the reachability probability to one, and
    # for every "bad" state, set it to zero
    for s in mdp['goodLeafNodes']:
        if setup['optimization_criterium'] == 'probability':
            # Maximize the reachability probability
            reward_vector[s] = 1
            
        else:
            # Maximize the expected reward (minimizing frequency deviations)
            freqs = mdp['states'][s]['state']['xg'][size_n.t : 2*size_n.t]
            
            # Determine reward for every bus frequency
            reward_vector[s] = calc_reward(setup['dfreq'], freqs)
            
    for s in mdp['badStates']:
        reward_vector[s] = 0
    
    # Loop over reserved time steps within optimization horizon
    for t in reverse_time_steps[1:]:
        
        print(' --- Solve MDP at time',t)
        
        # Retreive states in current time step
        current_states = [i for i,s in enumerate(mdp['states']) if 
                          s['time'] == t and i not in mdp['badStates']]
        
        # Loop over each of the current states
        for s in current_states:
            
            # Retreive current enabled action ID's
            current_actions = mdp['enabled_actions'][s]

            # Empty vector for probabilities
            if len(current_actions) > 0:
                current_rewards = [0]*(max(current_actions)+1)
                
                # Loop over every action
                for a in current_actions:
                    
                    # Determine which states could be reached using this action
                    reachable_states =      [key for key in mdp['trans'][s][a].keys()]
                    reachable_states_prob = [val for val in mdp['trans'][s][a].values()]
                    
                    if setup['optimization_criterium'] == 'probability':
                        # If we evaluate reachability, the only reward is given
                        # at the end of the optimization horizon
                        reward = 0
                    else:
                        # If the evaluate expected reward, we also consider
                        # rewards at every other step in the horizon
                        freqs = mdp['states'][s]['state']['xg'][size_n.t : 2*size_n.t]
                        
                        reward = calc_reward(setup['dfreq'], freqs)
                    
                    # Determine probability for taking the current action a, given
                    # by the sumproduct between the transition probabilities, and
                    # the reachability probability in that state s'
                    current_rewards[a] = reward + \
                        setup['discount_factor'] * (reachable_states_prob @ \
                        reward_vector[reachable_states])
                    
                # Determine the optimal action, by maximizing the probability
                reward_vector[s] = np.max(current_rewards)
                
                # Avoid using argmax, since we want to return a list of actions
                # if the optimal policy is not unique
                policy_vector[s] = _all_argmax(current_rewards)
                
            else:
                # No policy known
                reward_vector[s] = 0
            
    return policy_vector, reward_vector

def act_and_reduce(size_n, mdp, results, time_steps, actions):
    '''
    Perform the action prescribed by the optimal policy, proceed in time, and
    reduce the existing model by eliminating other branches

    Parameters
    ----------
    size_n : object
        Object containing network size values
    mdp : dict
        Dictionary containing all MDP model information
    result : dict
        Dictionary containing all simulation results
    time_steps : list
        List of datetime values corresponding to the time horizon
    actions : list
        List of integer values of the optimal policy at the current 
        
    Returns
    -------
    mdp_copy : dict
        Reduced MDP model to be used for the next iteration
    results : dict
        Update results dictionary (with the current results added)
    

    '''

    # Obtain the current and next time steps from the list of time steps
    t       = time_steps[0]
    t_next  = time_steps[1]
    
    # Select the optimal action (middle entry from list if multiple actions
    # are optimal)
    optimal_action = actions[ int(len(actions)/2) ]
    
    # Store the optimal action in the results
    results['mdp']['action'].loc[t] = optimal_action
    
    print(' --- Action to be taken is:',optimal_action)
    
    # Take the prescribed action and store the input/state results
    rnd = np.random.rand()
    successor_states = [key for key in mdp['trans'][0][optimal_action].keys()]
    successor_probabilities = [val for val in mdp['trans'][0][optimal_action].values()]

    # By default, the successor is a bad state
    successor = 'Bad state'
    # Loop over the successor state probabilities to see if we can end up
    # in a good state
    for z in range(len(successor_states)):
        rnd -= successor_probabilities[z]
        if rnd <= 0:
            successor = successor_states[z]
            break
    
    if successor == 'Bad state':
        # If successor is a bad state, abort the function
        print(' --- Successor state violated the constraints, so abort')
    else:
        print(' --- Successor state is',successor)
        
    # Retreive actual control inputs
    dP_disp         = mdp['control_input'][successor]['ug'][0 : size_n.g]
    R               = mdp['control_input'][successor]['ug'][size_n.g : 2*size_n.g]
    P_conv          = mdp['control_input'][successor]['ug'][2*size_n.g : 2*size_n.g + size_n.t]
    P_stor          = mdp['control_input'][successor]['us'][0::2]
    S_stor          = mdp['control_input'][successor]['us'][1::2]
    wind_forecast   = mdp['control_input'][successor]['wind_forecast']
    wind_error      = mdp['control_input'][successor]['wind_error']
    
    print(' --- Storage flexiblity deployed is:',S_stor,'MW')
    print(' --- Reserve deployment is:',R,'MW')
    print(' --- Wind power error is:',wind_error,'MW')
    
    # Store results in results dictionary
    results['x']['g'].loc[t_next]           = mdp['states'][successor]['state']['xg']
    results['x']['s'].loc[t_next]           = mdp['states'][successor]['state']['xs']
    results['x']['wind_error_state'].loc[t] = mdp['states'][successor]['state']['wind_error']
    
    results['grid']['dP_disp'].loc[t]       = dP_disp
    results['grid']['R'].loc[t]             = R
    results['grid']['P_conv'].loc[t]        = P_conv
    
    results['stor']['P_stor'].loc[t]        = P_stor
    results['stor']['S_stor'].loc[t]        = S_stor
    
    results['wind']['wind_forecast'].loc[t] = wind_forecast
    results['wind']['wind_error'].loc[t]    = wind_error
    
    results['mdp']['nr_states'].loc[t]      = mdp['nr_states']
    results['mdp']['nr_actions'].loc[t]     = mdp['nr_actions']
    
    # Convert good/bad state lists to boolean lists for indexing purposes
    badStatesBool = [False]*mdp['nr_states']
    for i in mdp['badStates']:
        badStatesBool[i] = True
        
    goodLeafNodesBool = [False]*mdp['nr_states']
    for i in mdp['goodLeafNodes']:
        goodLeafNodesBool[i] = True

    # Determine the list of states that should not be removed
    boolIdx = np.array([True if len(s['trace']) > 0 and 
        s['trace'][0] == optimal_action and
        s['windtrace'][0] == mdp['states'][successor]['state']['wind_error']
        else False for i,s in enumerate(mdp['states'])], dtype=bool)
    
    # Determine a new state ID for every current state (-1 means it will be
    # deleted)
    newIdxList = np.array([-1]*mdp['nr_states'])
    newIdxList[boolIdx] = np.arange(sum(boolIdx))
    
    print(' ----- Copying',sum(boolIdx),'states')
    
    # Reduce the tree from new starting point
    mdp_copy = dict()
    
    # Use the boolean index list to reduce model variables
    mdp_copy['nr_states']       = sum(boolIdx)
    mdp_copy['states']          = list(compress(mdp['states'], boolIdx))
    mdp_copy['enabled_actions'] = list(compress(mdp['enabled_actions'], boolIdx))
    mdp_copy['control_input']   = list(compress(mdp['control_input'], boolIdx))
    
    goodLeafNodesBool           = list(compress(goodLeafNodesBool, boolIdx))
    badStatesBool               = list(compress(badStatesBool, boolIdx))
    
    # Define new empty transition dictionary
    mdp_copy['trans']           = dict()
    
    # Copy or delete basic elements from transition dictionary, by looping
    # through all (state,action,state') pairs
    for s_key,s in mdp['trans'].items():
        
        # Delete or update the curent transition
        if boolIdx[s_key] == True:
            
            # Add entry in new dictionary using updated state number
            newIdx = newIdxList[s_key]
            mdp_copy['trans'][newIdx] = s
            
            for a_key,a in mdp_copy['trans'][newIdx].items():
                for s2_key,s2 in sorted(a.items()):
                    
                    # Determine new index for resulting state number
                    newIdx2 = newIdxList[s2_key]
                    
                    # Update current entry
                    mdp_copy['trans'][newIdx][a_key][newIdx2] = \
                        mdp_copy['trans'][newIdx][a_key].pop(s2_key)
                        
    # Correct number of actions copied
    mdp_copy['nr_actions'] = len(mdp_copy['trans'])
                        
    print(' ----- Copying',len(mdp_copy['trans']),'actions')
    
    # For every remaining state, remove the first entry of the trace
    for i,s in enumerate(mdp_copy['states']):
        mdp_copy['states'][i]['trace'] = mdp_copy['states'][i]['trace'][1:]
        mdp_copy['states'][i]['windtrace'] = mdp_copy['states'][i]['windtrace'][1:]
    
    # Convert boolean lists back to lists of good and bad state ID's
    mdp_copy['goodLeafNodes']   = [i for i in range(mdp_copy['nr_states'])
                                   if goodLeafNodesBool[i] is True]
    mdp_copy['badStates']       = [i for i in range(mdp_copy['nr_states'])
                                   if badStatesBool[i] is True]
        
    return mdp_copy, results