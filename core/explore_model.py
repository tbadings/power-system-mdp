# -*- coding: utf-8 -*-

import numpy as np

def _feasible_action_list(action_list, powBalanceMinFreq, powBalanceMaxFreq,
                          P_wf_vec, deltaP_w_MW_list, constr, B_sim_part,
                          h_sim, P_disp, P_conv):
    '''
    Takes the full action list and reduces it to the list of only the feasible
    actions
    
    Parameters
    ----------
    action_list : list of tuples
        List of tuples of all possible discrete actions
    powBalanceMinFreq : 1d array
    powBalanceMaxFreq : 1d array
    P_wf_vec : 1d array
        Vector of the wind power forecast
    deltaP_w_MW_list : 1d list
        List of possible successor states for the wind (converted to MW)
    constr : dict
        Shortcut to the TSO model constraint parameters
    B_sim_part : 2d array
        Actuation matrix for the grid model
    h_sim : float
        Time discretization step width in seconds
    P_disp : 1d array
        Current power dispatch state for each generator
    P_conv : 1d array
        Conventional (uncontrollable) power consumption for each node
    
    Returns
    ----------
    action_list[boolFull] : list of tuples
        List of tuples of feasible actions, given current state and conditions
    '''
    
    # Summed flexibility loads
    S             = action_list.T
    
    # print(S)
    
    # Define vector of ones with length of the number of actions
    action_length = len(action_list)
    # print('actions:',action_length)
    repOnesVec    = np.ones(action_length)
    
    # Repeat conventional consumption for every control in list the action list
    P_conv      = np.tile(P_conv, (action_length, 1)).T
    
    minWind     = min(deltaP_w_MW_list)
    S_minWind   = np.sum(S, axis=0) - minWind
    maxWind     = max(deltaP_w_MW_list) 
    S_maxWind   = np.sum(S, axis=0) - maxWind
    
    R_minWind = [max( min(float(S_minWind[i]), constr['R_us'][0] ), -constr['R_ds'][0] ) 
                 for i in range(action_length)]
    R_maxWind = [max( min(float(S_maxWind[i]), constr['R_us'][0] ), -constr['R_ds'][0] ) 
                 for i in range(action_length)]
    
    # Calculate ramp in power production needed
    needed_delta = repOnesVec*(np.sum(P_conv, axis=0) - P_wf_vec - P_disp) / h_sim
    dP_disp = [max( min(float(needed_delta[i]), constr['dP_GR_max'][0] ), constr['dP_GR_min'][0] ) 
                 for i in range(action_length)]
    
    # Calculate resulting power production at time step k+1
    power_dispatch = (P_disp + np.array(dP_disp) * h_sim)

    # Calculate weighted power balance in the grid for every control in list
    # The minimum balance scenario corresponds to the maximum wind. I.e., if 
    # for a given control and the scenario with maximum wind, the minimum 
    # frequency limit is already violated, then it will definitely be violated
    # for other wind power successor states.
    # The same reasoning holds for the maximum balance scenario, but then with
    # minimum wind power.
    
    balanceMaxWind = B_sim_part @ np.vstack((dP_disp, R_maxWind, P_conv, S))
    balanceMinWind = B_sim_part @ np.vstack((dP_disp, R_minWind, P_conv, S))
    
    # Boolean filters
    boolDispatch = (power_dispatch >= constr['P_GR_min'] + constr['R_ds']) & \
                   (power_dispatch <= constr['P_GR_max'] - constr['R_us'])
    
    boolBalance  = (balanceMaxWind >= np.transpose([powBalanceMinFreq]*action_length)) & \
                   (balanceMinWind <= np.transpose([powBalanceMaxFreq]*action_length))
    
    # Take vertical sum
    boolBalance  = np.all(boolBalance, axis=0)

    boolFull     = boolDispatch & boolBalance
    
    return action_list[boolFull]

def _eliminate_actions(setup, t, i, sys, xg0, xs0, action_list,
                                 P_wf_vec, deltaP_w_MW_list, P_conv):
    '''
    Function to eliminate actions which are physically not allowed
    Returns the reduced action space of allowed actions
    
    Parameters
    ----------
    setup : dict
        Dictionary containing all settings
    t : datetime
        Current simulation time
    i : int
        Current simulation iteration (corresponding to current t)
    sys : dict
        Dictionary containing all model data
    xg0 : 1d array
        Initial grid state
    xs0 : 1d array
        Initial storage state
    action_list: list of tuples
        List of tuples of all possible discrete actions
    P_wf_vec : 1d array
        Vector of the wind power forecast value
    deltaP_w_MW_list : 1d list
        List of possible successor states for the wind (converted to MW)
    P_conv : 1d array
        Conventional (uncontrollable) power consumption for each node
    
    Returns
    ----------
    u_space : list of tuples
        List of tuples of feasible actions, given current state and conditions
    '''
    
    # Shortcut to model size parameters
    n = sys['size']
    
    if i <= setup['initialization_time']:
        freqMin = [-10] * sys['size'].t
        freqMax = [10] * sys['size'].t
    else:
        freqMin = sys['grid']['constr']['freqMin']
        freqMax = sys['grid']['constr']['freqMax']
    
    ### GRID CONSTRAINTS
    # powBalanceMin <= B*(dP_disp + R - P_conv - S) <= powBalanceMax
    # The minimum power balance allowed is based on the maximum possible value
    # of the wind power, while the maximum balance is based on the minimum
    # wind power value    
    
    powBalanceMinFreq = freqMin*np.array(2*np.pi) - \
        (sys['dgrid']['A_sim'] @ xg0 +
        sys['dgrid']['C_sim'] @ (P_wf_vec + max(deltaP_w_MW_list)) )[n.t : 2*n.t]
        
    powBalanceMaxFreq = freqMax*np.array(2*np.pi) - \
        (sys['dgrid']['A_sim'] @ xg0 +
        sys['dgrid']['C_sim'] @ (P_wf_vec + min(deltaP_w_MW_list)) )[n.t : 2*n.t]
    
    ### STORAGE CONSTRAINTS
    # S_storMin <= S_stor <= S_storMax
    coefficient_stor = [sys['dstor']['B_sim'][i, 2*i+1] for i in range(n.s) ]
    xs1 = (sys['dstor']['A_sim'] @ xs0)
    
    S_storMin = (sys['stor']['constr']['x_s_min'] - xs1) / coefficient_stor
    S_storMax = (sys['stor']['constr']['x_s_max'] - xs1) / coefficient_stor
        
    # Define reduced lists for every control variable
    action_list_reduced = dict()
    
    action_list_reduced['S_stor'] = [tup for tup in action_list['S_stor'] if 
                                all([tup[i] >= S_storMin[i] for i in range(n.s)]) and
                                all([tup[i] <= S_storMax[i] for i in range(n.s)]) ]
    
    P_disp = np.sum(xg0[2*n.t:])
    
    B_sim_part = sys['dgrid']['B_sim'][ n.t : 2*n.t , : ]
    
    ### EVALUATE IF ANY CONSTRAINT IS VIOLATED
    u_space = _feasible_action_list( np.array(action_list_reduced['S_stor']), 
              powBalanceMinFreq, powBalanceMaxFreq, P_wf_vec, deltaP_w_MW_list,
              sys['grid']['constr'], B_sim_part, setup['time']['h_sim'], 
              P_disp, P_conv) 
    
    # print('u_space:',u_space)
        
    if setup['verbose']:
        origLength = len(action_list['S_stor'])
        print('\nEliminated',origLength-len(u_space),'of',origLength,'actions')
    
    return u_space

def _evaluate_state(setup, i, sys, xg, xs):
    '''
    Evaluate if a state violated any constraint
    Return TRUE if the state is "good", and FALSE if the state is "bad"
        
    Parameters
    ----------
    sys : dict
        Dictionary containing all model data
    xg0 : 1d array
        Grid state to evaluate
    xs0 : 1d array
        Storage state to evaluate
    
    Returns
    ----------
    goodState : boolean
        TRUE if the state is "good", and FALSE when the state is "bad"
    '''
    
    # Shortcut to model size parameters
    n = sys['size']
    
    goodState = True
    
    power_dispatch = xg[2*n.t :]
    bus_frequencies = xg[n.t : 2*n.t] / (2*np.pi)
    storage_SoC = xs
    
    # Within the initialization period, no frequency limits are imposed
    if i <= setup['initialization_time']:
        freqMin = [-10] * sys['size'].t
        freqMax = [10] * sys['size'].t
    else:
        freqMin = sys['grid']['constr']['freqMin']
        freqMax = sys['grid']['constr']['freqMax']
    
    # - Maximum power generation
    if any(f < sys['grid']['constr']['P_GR_min'][i] + sys['grid']['constr']['R_ds'] or
           f > sys['grid']['constr']['P_GR_max'][i] - sys['grid']['constr']['R_us']
           for i,f in enumerate(power_dispatch)):
        
        goodState = False
        if setup['verbose']:
            print(' -- Bad generator dispatch:',power_dispatch)
    
    # - Grid frequency deviations
    elif any(f < freqMin[i] or
           f > freqMax[i]
           for i,f in enumerate(bus_frequencies)):
        
        goodState = False
        if setup['verbose']:
            print(' -- Bad bus frequency:',bus_frequencies,'Hz')
    
    # - Storage SoC limits
    elif any(f < sys['stor']['constr']['x_s_min'][i] or
           f > sys['stor']['constr']['x_s_max'][i] 
           for i,f in enumerate(storage_SoC)):
        
        goodState = False
        if setup['verbose']:
            print(' -- Bad storage SoC:',storage_SoC)
    
    # - Line power limits
    if len(sys['grid']['L']) > 0:
        line_power_flow = sys['grid']['mat']['Laplacian'] @ xg[0:n.t]
        if any(f < sys['grid']['constr']['Lmin'][i] or
           f > sys['grid']['constr']['Lmax'][i] 
           for i,f in enumerate(line_power_flow)):
            
            goodState = False
            if setup['verbose']:
                print(' -- Bad line power flow:',line_power_flow)
    
    if setup['verbose']:
        if goodState is True:
            print(' >> Good state')
    
    return goodState

def explore_tree(setup, M, time_steps, i, sys, trace, windtrace, xg0, xs0, Pw_state, stateID):
    '''
    Explore a new node in the state-space exploration tree.
    Recursively calls itself until the full (feasible) tree has been explored
        
    Parameters
    ----------
    setup : dict
        Dictionary containing all settings
    M : dict
        Dictionary to save exploration results
    time_steps : list
        List of remaining simulation time steps
    sys : dict
        Dictionary containing all model data
    trace: list
        Action trace list of current evaluation
    windtrace: list
        Wind trace list of current evaluation    
    xg0 : 1d array
        Grid state to evaluate
    xs0 : 1d array
        Storage state to evaluate
    Pw_state : int
        Current discrete state of the wind power
    stateID : int
        ID of the current MDP state
        
    
    Returns
    ----------
    M : dict
        Dictionary of exploration results, with current results added
    '''
    
    # Retreive the current simulation time from the list of time steps
    t = time_steps[0]

    # Shortcut to model size parameters
    n = sys['size']

    # Occasionally print the action trace
    if setup['verbose'] and len(trace) <= 1:
        print('Action history:',trace)
    
    ### First retreive parameters independent of the action or random variable
    # Miscellaneous load    
    P_misc          = sys['grid']['P_misc'][i, :]          

    # Conventional power storage (assumed zero in the current work)
    P_stor         = np.zeros(n.t)

    # Conventional total power consumption
    P_conv  = [P_stor[i] + P_misc[i] for i in range(n.t)]

    # Retreive wind power forecast and determine actual power values
    P_wf_vec    = [sys['wind']['Pw_f_vec'][i]]

    ### Determine wind power successor states    
    wind_succ_idx = sys['wind']['model']['successors'][Pw_state]

    if setup['verbose']:
        print('Wind successor states:',wind_succ_idx)
    
    # Determine possible wind power error values
    deltaP_w_pu_list = sys['wind']['model']['states'][wind_succ_idx]
    deltaP_w_MW_list = deltaP_w_pu_list * sys['wind']['base']
    
    ### Eliminate "bad" actions up-front
    action_list = sys['action_list']
    u_space_reduced = _eliminate_actions(setup, t, i, sys, xg0, xs0,
                                         action_list, P_wf_vec,
                                         deltaP_w_MW_list, P_conv)
    
    # Store the list of enabled actions for the current state
    M['enabled_actions'][stateID] = np.arange(len(u_space_reduced))
    
    # Create new transition dictionary for current state
    M['trans'][stateID] = dict()
    
    # Choose change in generation implicitly, to balance total demand
    dP_disp = np.array([ np.sum(P_conv) - np.sum(xg0[2*n.t:])
                         - np.sum(P_wf_vec) ]) / setup['time']['h_sim']
    
    # Perform calculations independent of the action outside of the loop
    xg1 = sys['dgrid']['A_sim'] @ xg0        
    xs1 = sys['dstor']['A_sim'] @ xs0
    
    # Loop over the reduced, feasible action space
    for action,entry in enumerate(u_space_reduced):
        
        # Increment number of actions in MDP
        M['nr_actions'] += 1
        
        # Retreive current control inputs
        S_stor      = np.array(entry[0:n.s])

        # Create new transition dictionary for current (state,action) pair          
        M['trans'][stateID][action] = dict()
        
        if setup['verbose']:
            print('----------')
        
        # Loop over possible wind power successor states
        for rel_idx,wind_succ_state in enumerate(wind_succ_idx):
            
            ### Retreive wind power transition
            # Determine actual value of wind power error
            deltaP_w_MW = deltaP_w_MW_list[rel_idx]      
            
            P_wind = [max(0, float(P_wf_vec + deltaP_w_MW))]
            
            if setup['verbose']:
                print('Evalute for wind power',P_wind)
            
            # Choose S_storage implicitly, to mitigate the wind power error
            R = [max( min( np.sum(S_stor) - np.sum(deltaP_w_MW), 
                    sys['grid']['constr']['R_us'][0] ),
                    -sys['grid']['constr']['R_ds'][0] )]
            
            # Define concatenated input vectors
            ug      = np.concatenate((dP_disp, R, P_conv, S_stor))
            us      = np.concatenate([[P_stor[i], S_stor[i]] for i in range(n.s)])
            
            ### Perform simulation
            # Simulate grid
            xg = xg1 + \
                 sys['dgrid']['B_sim'] @ (ug/sys['grid']['base']) + \
                 sys['dgrid']['C_sim'] @ (P_wind/sys['grid']['base'])
            
            # Simulate storage units
            xs = xs1 + sys['dstor']['B_sim'] @ us
            
            ### Evaluation of resulting state
            # Add the resulting state to the list and retreive the ID
            if M['nr_states'] >= len(M['states']):
                
                # If index out of range, append new rows
                M['states'].extend([None]*100)
                M['enabled_actions'].extend([[0]]*100)
                M['control_input'].extend([[0]]*100)
                
            # Define the resulting state and trace (with current action)
            state_prime = {'xg': tuple(xg), 
                           'xs': tuple(xs), 
                           'wind_error': wind_succ_state}
            trace_prime = trace + [action]
            windtrace_prime = windtrace + [wind_succ_state]
            
            # Store and update the states results
            M['states'][M['nr_states']] = {'state': state_prime, 
                                           'time': time_steps[1], 
                                           'timestep': i+1,
                                           'trace': trace_prime,
                                           'windtrace': windtrace_prime}
            state_primeID               = M['nr_states']

            # Store the actual controls
            M['control_input'][state_primeID] = {'ug': ug, 
                                                 'us': us, 
                                                 'wind_forecast': P_wf_vec,
                                                 'wind_error': deltaP_w_MW}
            M['nr_states']              += 1
    
            # Evaluate if the resulting state is a "bad" state    
            # If bad state reached, terminate this branch
            if _evaluate_state(setup, i, sys, xg, xs) is True:
                
                # Determine transition probability
                probability = sys['wind']['model']['P_tr'][Pw_state, wind_succ_state]            
                
                # If no bad state reached, save this transition
                M['trans'][stateID][action][state_primeID] = probability
                
                # If still more time steps to explore            
                if len(time_steps[1:]) > 1:
                    # Call exploration function again
                    if setup['verbose']:
                        print('Take action',action,'(of ',len(u_space_reduced),')')
                    M = explore_tree(setup, M, time_steps[1:], i+1, sys, trace_prime, windtrace_prime,
                         xg0=xg, xs0=xs, Pw_state=wind_succ_state, stateID = state_primeID)
                else:
                    # Otherwise, bottom of tree has been reached
                    M['goodLeafNodes'] += [state_primeID]
                    if setup['verbose']:
                        print('Good end state reached, ID:',state_primeID,
                              '; Trace:',trace_prime)
                    
            else:
                # If current state is a bad state, add it to the dictionary
                M['badStates'] += [state_primeID]
    
    return M