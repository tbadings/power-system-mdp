# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt

import pandas as pd
import numpy as np

from matplotlib import cm

def cm2inch(*tupl):
    '''
    Convert from cm to inches
    '''
    
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)

def plot_wind_transition(setup, P_tr):    
    '''
    Function to create the plot for the wind transition matrix of the DTMC

    Parameters
    ----------
    setup : dict
        Dictionary containing settings
    P_tr : 2d array
        Array containing the DTMC transision matrix

    Returns
    -------
    None.
    '''    

    data_2d = P_tr
    #
    # Convert it into an numpy array.
    #
    data_array = np.array(data_2d)
    #
    # Create a figure for plotting the data as a 3D histogram.
    #
    wind_fig = plt.figure(figsize=cm2inch(16.8,9))
    ax = wind_fig.add_subplot(111, projection='3d')
    #
    # Create an X-Y mesh of the same dimension as the 2D data. You can
    # think of this as the floor of the plot.
    #
    x_data, y_data = np.meshgrid( np.arange(data_array.shape[1]),
                                  np.arange(data_array.shape[0]) )
    #
    # Flatten out the arrays so that they may be passed to "ax.bar3d".
    # Basically, ax.bar3d expects three one-dimensional arrays:
    # x_data, y_data, z_data. The following call boils down to picking
    # one entry from each array and plotting a bar to from
    # (x_data[i], y_data[i], 0) to (x_data[i], y_data[i], z_data[i]).
    #
    
    x_data = x_data.flatten()
    y_data = y_data.flatten()
    z_data = data_array.flatten()
    
    # Define colormap
    cmap = cm.get_cmap('jet') # Get desired colormap - you can change this!
    max_height = np.max(y_data)   # get range of colorbars so we can normalize
    min_height = np.min(y_data)
    rgba = [cmap((k-min_height)/max_height) for k in y_data] 
    
    ax.bar3d( x_data,
              y_data,
              np.zeros(len(z_data)),
              0.8, 0.8, z_data,
              color=rgba, zsort='average')
    
    ax.view_init(elev=60., azim=150)
    
    ax.set_xlim(0,max(x_data))
    ax.set_ylim(0,max(y_data))
    ax.set_zlim(0,1)
    
    ax.set_xlabel('S')
    ax.set_ylabel('S\'')
    ax.set_zlabel('Probability')
    
    ax.set_xticks(np.arange(0,max(x_data)+1,10))
    ax.set_yticks(np.arange(0,max(y_data)+1,10))
    ax.set_zticks([0,0.5,1])
    
    filename = setup['folder']['output']+'/'+setup['datestring_start']+'_windtransition'
    wind_fig.savefig(filename+'.pdf')
    wind_fig.savefig(filename+'.png')
    
    #
    # Finally, display the plot.
    #
    plt.show()
    plt.pause(0.001)

def save_data(setup, results, it):
    '''
    Save the results from solving the power system MDP in Excel

    Parameters
    ----------
    setup : dict
        Dictionary containing settings
    results : dict
        Dictionary containing all results
    it : int
        Iteration number

    Returns
    -------
    None.
    '''
    
    output_file = setup['folder']['output']+'/' + \
        'iter' + str(it) + '_' + setup['files']['output']
    
    # Select time horizon for exporting results
    t_list = setup['time']['date_index_sim']
    
    # Create a Pandas Excel writer using XlsxWriter as the engine
    writer = pd.ExcelWriter(output_file, engine='xlsxwriter')
    
    # Create merged dataframes for different result types
    states_df = pd.concat([results['x']['g'].loc[t_list], 
                           results['x']['s'].loc[t_list],
                           results['wind']['wind_forecast'].loc[t_list],
                           results['wind']['wind_error'].loc[t_list],
                           results['x']['wind_error_state'].loc[t_list]], 
                          axis=1)
    
    controls_df = pd.concat([results['grid']['dP_disp'].loc[t_list],
                             results['grid']['R'].loc[t_list],
                             results['grid']['P_conv'].loc[t_list],
                             results['stor']['P_stor'].loc[t_list],
                             results['stor']['S_stor'].loc[t_list]], 
                            axis=1)
    
    mdp_df = pd.concat([results['time']['iteration'].loc[t_list],
                        results['mdp']['action'].loc[t_list],
                        results['mdp']['opt_objective'].loc[t_list]], 
                       axis=1)
    
    # Write each merged dataframe to a different worksheet
    states_df.to_excel(writer, sheet_name='States')
    controls_df.to_excel(writer, sheet_name='Controls')
    mdp_df.to_excel(writer, sheet_name='MDP results')

    # Close the Pandas Excel writer and output the Excel file.
    writer.save()
    
def save_plots(setup, sys, results, it):
    '''
    Create and save result plots

    Parameters
    ----------
    setup : dict
        Dictionary containing settings
    sys : dict
        Dictionary containing al info on the continuous model
    results : dict
        Dictionary containing all results
    it : int
        Iteration number

    Returns
    -------
    None.
    '''
    
    filename_start = 'iter' + str(it) + '_' + setup['datestring_start']
    
    ### GRID FREQUENCY PLOT
    freq_fig, freq_ax = plt.subplots(figsize=cm2inch(12.2*0.55,6.5*0.8))
    
    freq = results['x']['g'][['g_'+str(i) for 
                      i in range(sys['size'].t, 2*sys['size'].t)]] / (2*np.pi)
    
    freq.plot( ax=freq_ax, color='k' )
        
    freq_ax.legend().set_visible(False)
    freq_ax.set_xlabel('Time')
    freq_ax.set_ylabel('Frequency deviation [Hz]')
    # freq_ax.title.set_text('Grid frequency deviation')
        
    # Set figure properties and show
    freq_fig.tight_layout()
    freq_ax.grid(which='both', axis='y', linestyle='dotted')
    freq_ax.axhline(setup['dfreq']['min'], linestyle='--', color='k')
    freq_ax.axhline(setup['dfreq']['max'], linestyle='--', color='k')
    
    freq_ax.set_ylim(1.2*setup['dfreq']['min'], 1.2*setup['dfreq']['max'])

    # Set x-axis
    freq_ax.set_xlim(setup['time']['date_index_sim'][0], 
                     setup['time']['date_index_sim'][-1])

    plt.show()
    plt.pause(0.001)
    
    filename = setup['folder']['output']+'/' + filename_start + '_frequency'
    freq_fig.savefig(filename+'.pdf', bbox_inches='tight')
    freq_fig.savefig(filename+'.png', bbox_inches='tight')
    
    ### ANCILLARY SERVICE POWER BALANCE
    ancillary_fig, ancillary_ax = plt.subplots(figsize=cm2inch(12.2*0.55,6.5*0.8))
    
    results['wind']['wind_error'].plot( ax=ancillary_ax )
    results['grid']['R'].plot( ax=ancillary_ax )
    (-results['stor']['S_stor']).plot( ax=ancillary_ax )
    
    ancillary_ax.legend(["Wind power error","Reserve deployed",
                         "Flexibility deployed"])
    # ancillary_ax.legend().set_visible(False)
    
    # ancillary_ax.set_ylim(-2.1, 2.1)
    
    ancillary_ax.set_xlabel('Time')
    ancillary_ax.set_ylabel('Power [MW]')
    # ancillary_ax.title.set_text('Ancillary service deployment')
    
    # Set figure properties and show
    ancillary_fig.tight_layout()
    ancillary_ax.grid(which='both', axis='y', linestyle='dotted')

    # Set x-axis
    ancillary_ax.set_xlim(setup['time']['date_index_sim'][0], 
                          setup['time']['date_index_sim'][-1])

    plt.show()
    plt.pause(0.001)
    
    filename = setup['folder']['output']+'/'+ filename_start +'_ancillary'
    ancillary_fig.savefig(filename+'.pdf', bbox_inches='tight')
    ancillary_fig.savefig(filename+'.png', bbox_inches='tight')
    
    ### POWER BALANCE
    power_fig, power_ax = plt.subplots(figsize=cm2inch(12.2,5.8))
    
    gen_column = 'g_'+str(2*sys['size'].t)
    P_gen = results['x']['g'][gen_column] + \
        setup['time']['h_sim'] * results['grid']['dP_disp']['dP_disp']
    
    P_gen.plot( ax=power_ax )
    results['wind']['wind_forecast'].plot( ax=power_ax )
    results['grid']['P_conv'].sum(axis=1).plot( ax=power_ax )
    
    power_ax.legend(["Generator dispatch","Wind forecast","Power demand"],
                    loc='upper left')
    
    power_ax.set_ylim(0,53)
    
    # power_fig.legend().set_visible(True)
    power_ax.set_xlabel('Time')
    power_ax.set_ylabel('Power [MW]')
    # power_ax.title.set_text('Day-ahead power balance (based on forecast)')
    
    # Set figure properties and show
    power_fig.tight_layout()
    power_ax.grid(which='both', axis='y', linestyle='dotted')

    # Set x-axis
    power_ax.set_xlim(setup['time']['date_index_sim'][0], 
                      setup['time']['date_index_sim'][-1])

    plt.show()
    plt.pause(0.001)
    
    filename = setup['folder']['output']+'/'+ filename_start + '_day-aheadbalance'
    power_fig.savefig(filename+'.pdf', bbox_inches='tight')
    power_fig.savefig(filename+'.png', bbox_inches='tight')
    
    ### STORAGE STATE OF CHARGE PLOT
    stor_fig, stor_ax = plt.subplots(figsize=cm2inch(12.2,6))
    
    # Convert from mega-Joule to kilo-Watt-hour
    freq = results['x']['s'] / 3.6
    
    freq.plot( ax=stor_ax, color='k' )
        
    stor_ax.legend().set_visible(False)
    stor_ax.set_xlabel('Time')
    stor_ax.set_ylabel('State of charge deviation [kWh]')
    # stor_ax.title.set_text('Battery state of charge')
        
    # Set figure properties and show
    stor_fig.tight_layout()
    stor_ax.grid(which='both', axis='y', linestyle='dotted')
    stor_ax.axhline(sys['stor']['constr']['x_s_min'][0] / 3.6, linestyle='--', color='k')
    stor_ax.axhline(sys['stor']['constr']['x_s_max'][0] / 3.6, linestyle='--', color='k')
    
    stor_ax.set_ylim(0, 0.3*sys['stor']['constr']['x_s_max'][0]/3.6)

    # Set x-axis
    stor_ax.set_xlim(setup['time']['date_index_sim'][0], 
                     setup['time']['date_index_sim'][-1])

    plt.show()
    plt.pause(0.001)
    
    filename = setup['folder']['output']+'/'+ filename_start +'_SoC'
    stor_fig.savefig(filename+'.pdf', bbox_inches='tight')
    stor_fig.savefig(filename+'.png', bbox_inches='tight')
    
def save_outer_loop(setup, montecarlo):
    '''
    Save the results from solving the outer loop as Excel file

    Parameters
    ----------
    setup : dict
        Dictionary containing settings
    montecarlo : dict
        Dictionary containing the outerloop Monte Carlo simulation results

    Returns
    -------
    None.

    '''
    
    output_file = setup['folder']['output']+'/' + \
        'outerloop_' + setup['files']['output']
        
    # Create a Pandas Excel writer using XlsxWriter as the engine
    writer = pd.ExcelWriter(output_file, engine='xlsxwriter')
    
    # Write each dataframe to a different worksheet
    montecarlo['iteration_time'].to_excel(writer, sheet_name='Time')
    montecarlo['mdp_cost'].to_excel(writer, sheet_name='Sum of objectives')
    montecarlo['nr_states'].to_excel(writer, sheet_name='Nr of states')
    montecarlo['nr_actions'].to_excel(writer, sheet_name='Nr of actions')
    pd.Series(montecarlo['overall_cost']).to_excel(writer, sheet_name='Cum. freq. dev.')

    # Close the Pandas Excel writer and output the Excel file.
    writer.save()