# -*- coding: utf-8 -*-
"""
Module containing smaller ancillary functions called repeatedly by other functions
"""
import numpy as np
import time

class network_size(object):
    '''
    Object to create shortcuts to system size
    '''
    def __init__(self, ng, nt, nw, ns):
        self.g = ng
        self.t = nt
        self.w = nw
        self.s = ns
        
        return

def nchoosek(n, k):
    '''
    Binomial coefficient or all combinations
    n C k = n! / ( (n-k)! * k! )
    '''
    if k == 0:
        r = 1
    else:
        r = n/k * nchoosek(n-1, k-1)
    return round(r)

def TicTocGenerator():
    ''' 
    Generator that returns time differences 
    '''
    ti = 0           # initial time
    tf = time.time() # final time
    while True:
        ti = tf
        tf = time.time()
        yield tf-ti # returns the time difference

TicToc = TicTocGenerator() # create an instance of the TicTocGen generator

def toc(tempBool=True):
    ''' 
    Print current time difference 
    '''
    # Prints the time difference yielded by generator instance TicToc
    tempTimeInterval = next(TicToc)
    if tempBool:
        print( "Elapsed time: %f seconds.\n" %np.round(tempTimeInterval, 5) )
    else:
        return np.round(tempTimeInterval, 9)

def tic():
    ''' 
    Start time recorder 
    '''
    # Records a time in TicToc, marks the beginning of a time interval
    toc(False)

def calc_reward(dfreq_limits, freqs):
    '''
    Compute instantaneous reward, based on current frequency deviation
    
    Parameters
    ----------
    dfreq_limits : dict
        Dictionary containing frequency deviation limits
    freqs : 1d array
        Vector of the current frequency deviations
    
    Returns
    ----------
    reward : float
        Instantaneous reward
    '''

    reward = sum([1 - freq/(2*np.pi*dfreq_limits['max']) if freq > 0
                else 1 - freq/(2*np.pi*dfreq_limits['min'])
                for freq in freqs])
    
    return reward

def calc_freq_integral(width, freqs):
    '''
    Compute the integral of the rewards over the full simulation horizon
    
    Parameters
    ----------
    width : float
        Time step width (resolution)
    freqs : 1d array
        Flat array of all frequency deviations over the full simulation hor.
    
    Returns
    ----------
    reward : float
        Integral of rewards over the full simulation horizon
    '''
    
    reward = sum([abs(freq)/(2*np.pi*width) for freq in freqs])    
    
    return reward